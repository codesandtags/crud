CREATE DATABASE crud;
USE crud;

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS rol;

CREATE TABLE rol(
	idrol INT NOT NULL AUTO_INCREMENT,
	rol VARCHAR(30) UNIQUE NOT NULL,
	PRIMARY KEY(idrol)
);

INSERT INTO rol(rol) VALUES('Administrador');

CREATE table user(
	iduser INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(15) UNIQUE NOT NULL,
	password VARCHAR(32) NOT NULL,
	name	VARCHAR(15)	 NOT NULL,
	creation_date DATETIME NOT NULL,
	email VARCHAR(60) NOT NULL,
	idrol	INT NOT NULL,
	PRIMARY KEY(iduser),
	FOREIGN KEY (idrol) REFERENCES rol(idrol)
);

CREATE TABLE audit(
  idaudit INT NOT NULL AUTO_INCREMENT,
  type VARCHAR(256),
  method VARCHAR(256),
  arguments VARCHAR(256),
  returnvalue TEXT,
  user VARCHAR(32),
  elapsedtime LONG,
  registerdate DATETIME
);