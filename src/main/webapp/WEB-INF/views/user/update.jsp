<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User</title>
</head>
<body>
<h1><spring:message code="label.create" /></h1>

<c:if test="${not empty message}">
    <p class="bg-success">${message}</p>
</c:if>

<form:form method="POST" commandName="user" action="${pageContext.request.contextPath}/user/update">
    <form:input path="idUser" class="form-control" />
    <table class="table">
        <tbody>
        <tr>
            <td><spring:message  code="label.name"/></td>
            <td>
                <form:input path="name" class="form-control" placeholder="Nombre de usuario" />
                <form:errors path="name" cssStyle="color:red;" />
            </td>
        </tr>
        <tr>
            <td><spring:message  code="label.username"/> </td>
            <td>
                <form:input path="username" class="form-control" placeholder="Usuario" />
                <form:errors path="username" cssStyle="color:red;" />
            </td>
        </tr>
        <tr>
            <td><spring:message  code="label.password"/> </td>
            <td>
                <form:password path="password" class="form-control" placeholder="Password" />
                <form:errors path="password" cssStyle="color:red;" />
            </td>
        </tr>
        <tr>
            <td><spring:message  code="label.email"/> </td>
            <td>
                <form:input path="email" class="form-control" placeholder="Email" />
                <form:errors path="email" cssStyle="color:red;" />
            </td>
        </tr>
        <tr>
            <td><spring:message  code="label.rol"/> </td>
            <td>
                <form:select path="rol" class="form-control" multiple="true">
                    <form:option value="0" label="Selecciona un Rol" />
                    <form:options items="${roles}" itemValue="idRol" itemLabel="idRol"/>
                </form:select>
                <form:errors path="rol" cssStyle="color:red;" />
            </td>
        </tr>
        <tr>
            <td colspan="2" ><input type="submit" value="Crear" class="btn btn-default"/></td>
        </tr>
        </tbody>
    </table>
</form:form>

</body>
</html>