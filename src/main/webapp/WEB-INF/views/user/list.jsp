<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CRUD</title>
</head>
<body>
    <h1><spring:message code="label.list" /></h1>
	
	<c:if test="${not empty message}">
		<p class="bg-success">${message}</p>
	</c:if>
	
	<table class="table">
		<thead>
			<th>Id</th>
			<th>Nombre</th>
			<th>Usuario</th>
            <th>Clave</th>
            <th>Email</th>
            <th>Rol</th>
            <th>Fecha Creacion</th>
            <th>Opciones</th>
		</thead>
	<c:forEach items="${users}" var="user">
		<tr>
            <td>${user.idUser}</td>
            <td>${user.name}</td>
            <td>${user.username}</td>
            <td>${user.password}</td>
            <td>${user.email}</td>
            <td>${user.rol.rol}</td>
            <td>${user.creationDate}</td>
			<td>
				<a href="${pageContext.request.contextPath}/user/delete/${user.idUser}"><spring:message code="label.delete" /></a> |
				<a href="${pageContext.request.contextPath}/user/update/${user.idUser}"><spring:message code="label.update" /></a>
			</td>
		</tr>
	</c:forEach>
	</table>

</body>
</html>