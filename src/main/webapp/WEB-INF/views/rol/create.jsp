<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Rol</title>
</head>
<body>
	<h1><spring:message code="label.create" /></h1>
	
	<c:if test="${not empty message}">
		<p class="bg-success">${message}</p>
	</c:if>
	
	<form:form method="POST" commandName="rol" action="${pageContext.request.contextPath}/rol/create">
        <table class="table">
			<tbody>
				<tr>
					<td>Name</td>
					<td>
                        <form:input path="rol" class="form-control" placeholder="Nombre del Rol" />
						<form:errors path="rol" cssStyle="color:red;" />
					</td>
				</tr>
				<tr>
					<td colspan="2" ><input type="submit" value="Crear" class="btn btn-default"/></td>
				</tr>
			</tbody>
		</table>
	</form:form>

</body>
</html>