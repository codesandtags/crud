<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!doctype html>
<html lang="en" ng-app="crud">
<head>
	<title>Index del CRUD</title>
</head>
<body>
	<h1 id="greeting"><spring:message code="project.greetings"/> {{nombre}} desde AngularJS!</h1>
	<input type="text" name="nombre" id="nombre" ng-model="nombre"><br/>
</body>
</html>