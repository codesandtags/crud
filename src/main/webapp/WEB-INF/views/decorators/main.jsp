<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="es" ng-app>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title><decorator:title/></title>

    <!-- CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/navbar-fixed-top.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme.css"/>

</head>
<body>

    <!-- menu -->
    <!-- Fixed navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${pageContext.request.contextPath}/"><spring:message code="project.name"/></a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#"><spring:message code="label.home"/></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="label.menu.rol" /> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="${pageContext.request.contextPath}/rol/list"><spring:message code="label.list" /></a></li>
                            <li><a href="${pageContext.request.contextPath}/rol/create"><spring:message code="label.create" /></a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="label.menu.user" /> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="${pageContext.request.contextPath}/user/list"><spring:message code="label.list" /></a></li>
                            <li><a href="${pageContext.request.contextPath}/user/create"><spring:message code="label.create" /></a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li ><a href="?lang=es">es</a></li>
                    <li ><a href="?lang=en">en</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <decorator:body/>
        </div>

        <hr>

        <footer>
            <p><spring:message code="label.copyright" /></p>
        </footer>
    </div> <!-- /container -->

    <!-- Javascript -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/angular.min.js"></script>

</body>
</html>