package com.crud.validation;

import com.crud.model.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator{

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        User user = (User) target;

        ValidationUtils.rejectIfEmpty(errors, "username", "username.required");
        ValidationUtils.rejectIfEmpty(errors, "name", "name.required");
        ValidationUtils.rejectIfEmpty(errors, "password", "password.required");
        ValidationUtils.rejectIfEmpty(errors, "email", "email.required");

        if(user.getRol() == null || user.getRol().getIdRol() == 0){
            errors.rejectValue("rol", "user.rol.required");
        }

    }
}
