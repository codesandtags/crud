package com.crud.validation;

import com.crud.model.Rol;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class RolValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return Rol.class.isAssignableFrom(clazz);
	}

	/**
	 * Validaciones puntuales para la persistencia del Rol
	 */
	@Override
	public void validate(Object target, Errors errors) {
		
		Rol rol = (Rol) target;
		
		ValidationUtils.rejectIfEmpty(errors, "rol", "rol.required");
		
	}

}
