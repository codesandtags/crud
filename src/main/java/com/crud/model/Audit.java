package com.crud.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "audit")
public class Audit implements Serializable{

    @Id
    @GeneratedValue
    @Column(name = "idaudit")
    private Long idAudit;

    @Column(name = "type")
    private String type;

    @Column(name = "method")
    private String method;

    @Column(name = "arguments")
    private String arguments;

    @Column(name = "returnvalue")
    private String returnValue;

    @Column(name = "elapsedtime")
    private Long elapsedTime;

    @Column(name = "registerdate")
    private Date registerDate;

    @Column(name = "user")
    private String user;

    public Long getIdAudit() {
        return idAudit;
    }

    public void setIdAudit(Long idAudit) {
        this.idAudit = idAudit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(String returnValue) {
        this.returnValue = returnValue;
    }

    public Long getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(Long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getArguments() {
        return arguments;
    }

    public void setArguments(String arguments) {
        this.arguments = arguments;
    }
}
