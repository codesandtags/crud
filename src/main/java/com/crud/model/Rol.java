package com.crud.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Entity
@Table(name="rol")
public class Rol implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="idrol")
	private Long idRol;
	
	@Column(name="rol", unique = true)
	@NotNull @NotEmpty
    @Size(max = 20)
	private String rol;

	public Long getIdRol() {
		return idRol;
	}

	public void setIdRol(Long idRol) {
		this.idRol = idRol;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

    @Override
    public String toString(){
        return "[" + getIdRol() + ", " + getRol() + "]";
    }
}
