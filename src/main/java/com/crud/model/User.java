package com.crud.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "user")
public class User implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "iduser")
    private Long idUser;

    @JoinColumn(name = "idrol")
    @ManyToOne(fetch = FetchType.EAGER)
    private Rol rol;

    @Column(name = "username")
    @Size(max = 15)
    private String username;

    @Column(name = "password")
    @Size(max = 32)
    private String password;

    @Column(name = "name")
    @Size(max = 15)
    private String name;

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "email")
    @Size(max = 60)
    private String email;

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
