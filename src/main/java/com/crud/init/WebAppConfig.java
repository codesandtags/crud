package com.crud.init;

import com.crud.converter.RolConverter;
import org.hibernate.ejb.HibernatePersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.datetime.DateFormatterRegistrar;
import org.springframework.format.number.NumberFormatAnnotationFormatterFactory;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.*;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan("com.crud")
@PropertySource("classpath:crud.properties")
@EnableJpaRepositories("com.crud.repository")
@EnableAspectJAutoProxy
public class WebAppConfig extends WebMvcConfigurationSupport{
	
	private static final String PROPERTY_DATABASE_DRIVER 	= "db.driverClassName";
	private static final String PROPERTY_DATABASE_URL	 	= "db.url";
	private static final String PROPERTY_DATABASE_USERNAME 	= "db.username";
	private static final String PROPERTY_DATABASE_PASSWORD 	= "db.password";
	
	private static final String PROPERTY_HIBERNATE_DIALECT	= "rdbms.dialect";
	private static final String PROPERTY_SHOW_SQL			= "rdbms.show_sql";
	private static final String PROPERTY_PACKAGES_TO_SCAN	= "rdbms.packate_to_scan";
	private static final String PROPERTY_GENERATOR_MAPPINGS	= "rdbms.new_generator_mappings";
    private static final String PROPERTY_HIBERNATE_DDL      = "rdbms.hbmddl";

	@Resource
	private Environment env;

    @Autowired
    private RolConverter rolConverter;
	
	/**
	 * Configuracion del datasource de la aplicacion
	 * @return
	 * @author huntercode
	 */
	@Bean
	public DataSource dataSource(){
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
		dataSource.setDriverClassName(env.getRequiredProperty(PROPERTY_DATABASE_DRIVER));
		dataSource.setUrl(env.getRequiredProperty(PROPERTY_DATABASE_URL));
		dataSource.setUsername(env.getRequiredProperty(PROPERTY_DATABASE_USERNAME));
		dataSource.setPassword(env.getRequiredProperty(PROPERTY_DATABASE_PASSWORD));
		
		return dataSource;
	}
	
	/**
	 * Configuracion del contenedor para el EntityManager
	 * @return
	 * @author huntercode
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(){
		
		LocalContainerEntityManagerFactoryBean myEmf = new LocalContainerEntityManagerFactoryBean();
		myEmf.setDataSource(dataSource());
		myEmf.setPersistenceProviderClass(HibernatePersistence.class);
		myEmf.setPackagesToScan(env.getRequiredProperty(PROPERTY_PACKAGES_TO_SCAN));
		myEmf.setJpaProperties(hibernateProperties());
		
		return myEmf;
	}
	
	/**
	 * Configuracion de propiedades de Hibernate para JPA
	 * @return
	 * @author huntercode
	 */
	private Properties hibernateProperties(){
		Properties properties = new Properties();
		properties.put("hibernate.dialect", env.getProperty(PROPERTY_HIBERNATE_DIALECT));
		properties.put("hibernate.show_sql", env.getProperty(PROPERTY_SHOW_SQL));
		properties.put("entitymanager.packages.to.scan", env.getProperty(PROPERTY_PACKAGES_TO_SCAN));
        properties.put("hibernate.hbm2ddl.auto", env.getProperty(PROPERTY_HIBERNATE_DDL));

		return properties;
	}
	
	/**
	 * Configuracion del transactionManager de JPA
	 * @return
	 * @author huntercode
	 */
	@Bean
	public JpaTransactionManager transactionManager(){
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}
	
	
	/**
	 * Configuración del manejador de las vistas
	 * 
	 * @return
	 * @author huntercode
	 */
	@Bean
	public UrlBasedViewResolver setupViewResolver(){
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}
	
	/**
	 * Configuracion de mensajes de la aplicacion
	 * @return
	 * @author huntercode
	 */
	@Bean
	public ResourceBundleMessageSource messageSource(){
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasenames(env.getProperty("message.source.basename"));
		source.setUseCodeAsDefaultMessage(true);
		return source;
	}

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor(){
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Bean
    public SessionLocaleResolver sessionLocaleResolver(){
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(new Locale("es"));
        return slr;
    }

    @Bean
    public LocaleResolver localeResolver(){
        return sessionLocaleResolver();
    }

    @Bean
    public RequestMappingHandlerMapping requestMappingHandlerMapping(){
        RequestMappingHandlerMapping requestMapping = new RequestMappingHandlerMapping();
        requestMapping.setInterceptors(new Object[]{localeChangeInterceptor()});
        return requestMapping;
    }


    @Bean
    public RequestMappingHandlerAdapter requestMappingHandlerAdapter() {
        ConfigurableWebBindingInitializer webBindingInitializer = new ConfigurableWebBindingInitializer();
        webBindingInitializer.setConversionService(conversionService());
        webBindingInitializer.setValidator(mvcValidator());
        webBindingInitializer.setAutoGrowNestedPaths(false);

        List<HandlerMethodArgumentResolver> argumentResolvers = new ArrayList<HandlerMethodArgumentResolver>();
        addArgumentResolvers(argumentResolvers);

        List<HandlerMethodReturnValueHandler> returnValueHandlers = new ArrayList<HandlerMethodReturnValueHandler>();
        addReturnValueHandlers(returnValueHandlers);

        RequestMappingHandlerAdapter adapter = new RequestMappingHandlerAdapter();
        adapter.setMessageConverters(getMessageConverters());
        adapter.setWebBindingInitializer(webBindingInitializer);
        adapter.setCustomArgumentResolvers(argumentResolvers);
        adapter.setCustomReturnValueHandlers(returnValueHandlers);
        return adapter;
    }

    @Bean
    public FormattingConversionService conversionService() {

        // Use the DefaultFormattingConversionService but do not register defaults
        DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService(false);

        // Ensure @NumberFormat is still supported
        conversionService.addFormatterForFieldAnnotation(new NumberFormatAnnotationFormatterFactory());

        // Register date conversion with a specific global format
        DateFormatterRegistrar registrar = new DateFormatterRegistrar();
        registrar.setFormatter(new DateFormatter("yyyyMMdd"));
        registrar.registerFormatters(conversionService);

        conversionService.addConverter(rolConverter);

        return conversionService;
    }

}
