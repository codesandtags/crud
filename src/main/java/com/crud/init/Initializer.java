package com.crud.init;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

/**
 * Clase que inicializa el contexto de la aplicacion, utilizando la configuracion de la clase
 * WebAppConfig
 * * @author huntercode
 *
 */
public class Initializer implements WebApplicationInitializer{

	public static final String DISPATCHER_SERVLET_NAME = "mvc-dispatcher";
    public static final Logger LOGGER = LoggerFactory.getLogger(Initializer.class);
	
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		
		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
		ctx.register(WebAppConfig.class);
		servletContext.addListener(new ContextLoaderListener(ctx));
		
		ctx.setServletContext(servletContext);
		
		Dynamic servlet = servletContext.addServlet(DISPATCHER_SERVLET_NAME, new DispatcherServlet(ctx));
		servlet.addMapping("/");
		servlet.setLoadOnStartup(1);

        LOGGER.info("Aplicacion iniciada correctamente!! :D");
	}

}