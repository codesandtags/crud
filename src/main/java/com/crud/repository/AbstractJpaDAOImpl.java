package com.crud.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

/**
 * Template JPA
 *
 * @param <T>
 */
public abstract class AbstractJpaDAOImpl <T extends Serializable> implements AbstractJpaDAO< T >{
	
	private Class< T > clazz;
	
	@PersistenceContext
	EntityManager entityManager;
	
	public final void setClass( Class< T > clazzToSet){
		this.clazz = clazzToSet;
	}
	
	public void create( T entity){
		entityManager.persist(entity);
	}
	
	public void delete( T entity){
		entityManager.remove(entity);
	}
	
	public void deleteById( Long id ){
		T entity = findById(id);
		delete(entity);
	}
	
	public T update( T entity){
		return entityManager.merge(entity);
	}
	
	public T findById(Long id){
		return entityManager.find(clazz, id);
	}
	
	@SuppressWarnings("unchecked")
	public List< T > findAll(){
		return entityManager.createQuery("from " + clazz.getName()).getResultList();
	}	 

}
