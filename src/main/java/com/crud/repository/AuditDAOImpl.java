package com.crud.repository;

import com.crud.model.Audit;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class AuditDAOImpl extends AbstractJpaDAOImpl<Audit> implements AuditDAO {
}
