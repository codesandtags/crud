package com.crud.repository;

import com.crud.model.User;

public interface UserDAO extends AbstractJpaDAO<User>{

}
