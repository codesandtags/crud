package com.crud.repository;

import com.crud.model.Rol;
import org.springframework.stereotype.Repository;

@Repository
public class RolDAOImpl extends AbstractJpaDAOImpl<Rol> implements RolDAO{

	public RolDAOImpl() {
		setClass(Rol.class);
	}

}
