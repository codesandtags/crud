package com.crud.repository;

import java.util.List;

/**
 * Template JPA
 *
 * @param <T>
 */
interface AbstractJpaDAO < T > {
	
	void create( T entity);
	
	void delete( T entity);
	
	void deleteById( Long id );
	
	T update( T entity);
	
	T findById(Long id);
	
	List< T > findAll();

}
