package com.crud.repository;

import com.crud.model.Rol;

public interface RolDAO extends AbstractJpaDAO<Rol>{
	
}
