package com.crud.repository;

import com.crud.model.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl extends AbstractJpaDAOImpl<User> implements UserDAO{

    public UserDAOImpl(){ setClass(User.class); }

}
