package com.crud.service;

import com.crud.exception.UserNotFound;
import com.crud.model.User;

import java.util.List;

public interface UserService {

    void create(User user);
    void update(User user) throws UserNotFound;
    void delete(Long id) throws UserNotFound;
    User findById(Long id)  throws UserNotFound;
    List<User> listAll();

}
