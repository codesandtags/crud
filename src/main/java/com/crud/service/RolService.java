package com.crud.service;

import com.crud.exception.RolNotFound;
import com.crud.model.Rol;

import java.util.List;

public interface RolService {
	
	void create(Rol rol);
	void update(Rol rol) throws RolNotFound;
	void delete(Long id) throws RolNotFound;
	Rol findById(Long id)  throws RolNotFound;
	List<Rol> listAll();

}
