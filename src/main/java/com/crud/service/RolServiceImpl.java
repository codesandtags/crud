package com.crud.service;

import com.crud.exception.RolNotFound;
import com.crud.model.Rol;
import com.crud.repository.RolDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RolServiceImpl implements RolService{

	@Autowired
	private RolDAO rolDAO;
	
	@Override
	@Transactional
	public void create(Rol rol) {
		rolDAO.create(rol);
	}

	@Override
	@Transactional(rollbackFor=RolNotFound.class)
	public void update(Rol rol) throws RolNotFound {
		
		Rol rolUpdate = rolDAO.findById(rol.getIdRol());
		
		if(rolUpdate == null)
			throw new RolNotFound();
		
		rolUpdate.setRol(rol.getRol());
		
		rolDAO.update(rolUpdate);
	}

	@Override
	@Transactional(rollbackFor=RolNotFound.class)
	public void delete(Long id) throws RolNotFound{
		Rol rol = rolDAO.findById(id);
		
		if(rol == null)
			throw new RolNotFound();
		
		rolDAO.delete(rol);
	}

	@Override
	public Rol findById(Long id) throws RolNotFound {

        Rol rol = rolDAO.findById(id);

        if(rol == null)
            throw new RolNotFound("No se pudo encontrar rol con id " + id);

        return rol;
	}

	@Override
	@Transactional
	public List<Rol> listAll() {
		return rolDAO.findAll();
	}

}
