package com.crud.service;

import com.crud.exception.UserNotFound;
import com.crud.model.User;
import com.crud.repository.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public void create(User user) {
        user.setCreationDate(Calendar.getInstance().getTime());
        userDAO.create(user);
    }

    @Override
    @Transactional(rollbackFor=UserNotFound.class)
    public void update(User user) throws UserNotFound{

        User userUpdate = userDAO.findById(user.getIdUser());

        if(userUpdate == null)
            throw new UserNotFound();

        userUpdate.setRol(user.getRol());

        userDAO.update(userUpdate);
    }

    @Override
    @Transactional(rollbackFor=UserNotFound.class)
    public void delete(Long id) throws UserNotFound{
        User user = userDAO.findById(id);

        if(user == null)
            throw new UserNotFound();

        userDAO.delete(user);
    }

    @Override
    public User findById(Long id) throws UserNotFound {

        User user = userDAO.findById(id);

        if(user == null)
            throw new UserNotFound();

        return user;
    }

    @Override
    @Transactional
    public List<User> listAll() {
        return userDAO.findAll();
    }



}
