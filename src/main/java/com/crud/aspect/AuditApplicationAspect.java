package com.crud.aspect;

import com.crud.model.Audit;
import com.crud.repository.AuditDAO;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;

@Aspect
@Component
public class AuditApplicationAspect {

    Logger LOGGER = LoggerFactory.getLogger(AuditApplicationAspect.class);

    @Autowired
    AuditDAO auditDAO;

    public static final String SERVICE_METHOD_POINTCUT = "execution(* com.crud.service.*.*(..))";
    public static final String CONTROLLER_METHOD_POINTCUT = "execution(* com.crud.controller.*.*(..))";

    @Pointcut(SERVICE_METHOD_POINTCUT)
    public void serviceMethod(){ }

    @Pointcut(CONTROLLER_METHOD_POINTCUT)
    public void controllerMethod(){ }

    @Around("serviceMethod() || controllerMethod()")
    public Object intercetpFacadeMethod(ProceedingJoinPoint pjp) throws Throwable{

        long start = System.currentTimeMillis();
        Object output = pjp.proceed();
        long elapsedTime = System.currentTimeMillis() - start;

        final Object[] logVars = new Object[]{pjp.getSignature().getDeclaringType(),
                            pjp.getSignature().getName(),
                            pjp.getArgs(),
                            elapsedTime,
                            output == null ? "null" : output.toString()

                            };
        LOGGER.info("Auditando!.... ");
        LOGGER.info("[ {} {} {} ] Time : [{} ms] Return [{}]", logVars);

        try {
            Audit auditRegister = new Audit();
            auditRegister.setType(pjp.getSignature().getDeclaringTypeName());
            auditRegister.setMethod(pjp.getSignature().getName());
            auditRegister.setArguments(pjp.getArgs().toString());
            auditRegister.setElapsedTime(elapsedTime);
            auditRegister.setReturnValue(output == null ? "null" : output.toString());
            auditRegister.setUser("N/A");
            auditRegister.setRegisterDate(Calendar.getInstance().getTime());
            auditDAO.create(auditRegister);
        }catch (Exception e){
            LOGGER.error("Error guardando registro de auditoria {} ", e);
        }

        return output;
    }

}
