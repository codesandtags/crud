package com.crud.exception;

public class RolNotFound extends Exception{

    public RolNotFound(){ super(); }
    public RolNotFound(String message){ super(message); }
	
}
