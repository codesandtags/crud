package com.crud.util;

public class GeneratorNames {
	
	String consonants = "bcdfghjklmnpqrstvwxys";
	String vowels	= "aeiou";
	
	public static void main(String[] args) {
		
		GeneratorNames generator = new GeneratorNames();
		
		for (int i = 0; i < 5; i++) {
			System.out.println("[" + i + "] " + generator.generateName("cvcvcv"));
		}
		System.out.println("------------------------");
		for (int i = 0; i < 5; i++) {
			System.out.println("[" + i + "] " + generator.generateName("cvvcv"));
		}
		System.out.println("------------------------");
		for (int i = 0; i < 5; i++) {
			System.out.println("["	 + i + "] " + generator.generateName("vcvcv"));
		}
		System.out.println("------------------------");
		for (int i = 0; i < 5; i++) {
			System.out.println("[" + i + "] " + generator.generateName("cvcvv"));
		}
		System.out.println("------------------------");
		for (int i = 0; i < 5; i++) {
			System.out.println("[" + i + "] " + generator.generateName("cvcv"));
		}
		
	}
	
	/**
	 * Genera nombres aleatoreos a partin de un patron definido
	 * 
	 *    c : Representa una consonante
	 *    a : Representa una vocal
	 *      : En caso de colocar algo diferente se deja la letra ingresada en esa posicion  
	 *    
	 * @param pattern
	 * @return {@link String} nombre generado
	 */
	public String generateName(String pattern){
		
		StringBuilder name = new StringBuilder();
		int random = 0;
		
		for (int i = 0; i < pattern.length(); i++) {
			
			switch(pattern.charAt(i)){
				case 'c':
					random = (int)(Math.random() * consonants.length());
					name.append(consonants.charAt(random));
					break;
					
				case 'v':
					random = (int)(Math.random() * vowels.length());
					name.append(vowels.charAt(random));
					break;
					
				default:
					name.append(pattern.charAt(i));
					break;
			}
		}
		
		return name.toString();
	}
}
