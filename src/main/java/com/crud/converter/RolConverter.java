package com.crud.converter;

import com.crud.model.Rol;
import com.crud.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RolConverter implements Converter<String, Rol>{

    @Autowired
    private RolService rolService;

    @Override
    public Rol convert(String s) {
        Rol rol = null;
        try {
            rol = rolService.findById(Long.parseLong(s));
        }catch (Exception e){
            e.printStackTrace();
        }
        return rol;
    }
}
