package com.crud.controller;

import com.crud.model.Rol;
import com.crud.service.RolService;
import com.crud.validation.RolValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value="/rol")
public class RolController {

	@Autowired
	private RolService rolService;
	
	@Autowired
	private RolValidator rolValidator;
	
	@InitBinder
	private void initBinder(WebDataBinder binder){
		binder.setValidator(rolValidator);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView listRoles() {
		ModelAndView mav = new ModelAndView("/rol/list");
		List<Rol> roles = rolService.listAll();
		mav.addObject("roles", roles);
		return mav;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView mav = new ModelAndView("/rol/create", "rol", new Rol());
		return mav;
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ModelAndView create(@ModelAttribute @Valid Rol rol,
								BindingResult result,
								final RedirectAttributes redirectAttributes) {
		
		if(result.hasErrors()){
			return new ModelAndView("/rol/create", "rol", rol);
		}
		
		ModelAndView mav = new ModelAndView("redirect:/rol/list");
		String message   = null;
		
		try{
			rolService.create(rol);
			message = "El rol ha sido creado";
			
		}catch(Exception e){
			message = "Error creando el rol";
			mav = new ModelAndView("/rol/create/");
			mav.addObject("rol", rol);
		}
		
		redirectAttributes.addFlashAttribute("message", message);
		return mav;
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable Long id,
								final RedirectAttributes redirectAttributes) {
		
        ModelAndView mav = new ModelAndView("redirect:/rol/list");          
		String message   = null;
		
		try{
			rolService.delete(id);
			message = "El Rol ha sido eliminado";
			
		}catch(Exception e){
            mav.setViewName("/rol/list");
			message = "Ocurrio un error eliminado el rol";
		}
		
		redirectAttributes.addFlashAttribute("message", message);  
		
		return mav;
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable Long id) {
		ModelAndView mav = new ModelAndView("/rol/update");
		try{
			Rol rol = rolService.findById(id);
			mav.addObject("rol", rol);
		}catch(Exception e){
            mav.setViewName("/rol/list");
			mav.addObject("message", "No se encontro el Rol");
		}
		return mav;
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ModelAndView update(@ModelAttribute @Valid Rol rol,
								BindingResult result,
								final RedirectAttributes redirectAttributes) {
		
		if(result.hasErrors()){
			return new ModelAndView("/rol/update", "rol", rol);
		}
		
		ModelAndView mav = new ModelAndView("redirect:/rol/list");          
		String message   = null;
		
		try{
			rolService.update(rol);
			message = "El Rol ha sido actualizado";
		}catch(Exception e){
			message = "Error actualizando el rol";
			mav = new ModelAndView("/rol/list");
			mav.addObject("rol", rol);
		}
		
		redirectAttributes.addFlashAttribute("message", message);
		return mav;
	}
	

}
