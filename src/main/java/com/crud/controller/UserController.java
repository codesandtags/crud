package com.crud.controller;

import com.crud.model.Rol;
import com.crud.model.User;
import com.crud.service.RolService;
import com.crud.service.UserService;
import com.crud.validation.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RolService rolService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private ConversionService conversionService;

    @InitBinder
    private void initBinder(WebDataBinder binder){
        binder.setValidator(userValidator);
        //binder.registerCustomEditor(Rol.class, "rol", new RolEditor(rolService));
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView mav = new ModelAndView("/user/list");
        mav.addObject("users", userService.listAll());
        mav.addObject("roles", rolService.listAll());
        return mav;
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {

        ModelAndView mav = new ModelAndView("/user/create", "user", new User());
        mav.addObject("roles", rolService.listAll());
        return mav;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView create(@ModelAttribute @Valid User user,
                               BindingResult result,
                               final RedirectAttributes redirectAttributes) {

        ModelAndView mav = new ModelAndView("redirect:/user/list");
        String message   = null;

        if(result.hasErrors()){
            mav = new ModelAndView("/user/create");
            mav.addObject("user", user);
            mav.addObject("roles", rolService.listAll());
            return mav;
        }

        try{
            userService.create(user);
            message = "El usuario ha sido creado";

        }catch(Exception e){
            message = "Error creando el usuario";
            mav = new ModelAndView("/user/create/");
            mav.addObject("user", user);
            mav.addObject("roles", rolService.listAll());
        }

        redirectAttributes.addFlashAttribute("message", message);
        return mav;
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable Long id,
                               final RedirectAttributes redirectAttributes) {

        ModelAndView mav = new ModelAndView("redirect:/user/list");
        String message   = null;

        try{
            userService.delete(id);
            message = "El usuario ha sido eliminado";

        }catch(Exception e){
            mav.setViewName("/user/list");
            message = "Ocurrio un error eliminado el usuario";
        }

        redirectAttributes.addFlashAttribute("message", message);

        return mav;
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public ModelAndView update(@PathVariable Long id) {
        ModelAndView mav = new ModelAndView("/user/update");
        try{
            User user = userService.findById(id);
            mav.addObject("user", user);
            mav.addObject("roles", rolService.listAll());
        }catch(Exception e){
            mav.setViewName("/user/list");
            mav.addObject("message", "No se encontro el usuario");
        }
        return mav;
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView update(@ModelAttribute @Valid User user,
                               BindingResult result,
                               final RedirectAttributes redirectAttributes) {

        ModelAndView mav = new ModelAndView("redirect:/user/list");
        String message   = null;

        if(result.hasErrors()){
            mav = new ModelAndView("/user/create");
            mav.addObject("user", user);
            mav.addObject("roles", rolService.listAll());
            return mav;
        }



        try{
            userService.update(user);
            message = "El usuario ha sido actualizado";
        }catch(Exception e){
            message = "Error actualizando el usuario";
            mav = new ModelAndView("/usuario/list");
            mav.addObject("user", user);
        }

        redirectAttributes.addFlashAttribute("message", message);
        return mav;
    }
}
